﻿#include <stdio.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <cstdlib>
#include <cmath>
#include <ctime>
/**
 * CUDA Kernel Device code
 *
 * Выполняет вычисление операции сложения векторов A и B в C. Все три вектора имеют одинаковый размер numElements.
 */
__global__ void // будет вызываться с хсота и выполняться на девайсе
vectorAdd(const float *A, const float *B, float *C, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x; // вычисляем индекс элемента, который будем вычилять этой нити
	//  размер блока потоков*(индекс блока потоков внутри решетки + индекс потока внутри блока потоков)
    if (i < numElements) // если индекс получился больше, чем размер массива, то ничего не делаем, чтобы случайно не перезаписать чужую память
    {
        C[i] = A[i] + B[i]; // вычисляем
    }
}

void vectorAddCPU(const float *A, const float *B, float *C, int numElements)
{
	for(int i=0;i<numElements;i++)
	{
		C[i] =  A[i]+B[i];
	}
}

/**
 * Host main routine
 */
int main(void)
{
    // Код ошибки для проверки ошибок, возвращённых CUDA вызовами
    cudaError_t err = cudaSuccess;
	cudaEvent_t start, stop;
	


    // Выводим длину веткора и считаем его размер
    int numElements = 150000;
    size_t size = numElements * sizeof(float);
    printf("[Vector addition of %d elements]\n", numElements);

    // Выделяем место под вектор A на хосте
    float *h_A = (float *)malloc(size);

	// Выделяем место под вектор B на хосте
    float *h_B = (float *)malloc(size);

	// Выделяем место под вектор C на хосте
    float *h_C = (float *)malloc(size);
	// Выделяем место под вектор C, вычисляемый на хосте
	float *h_C_CPU = (float *)malloc(size);

    // Проверяем, что выделение места успешно
    if (h_A == NULL || h_B == NULL || h_C == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Заполняем векторы А и В на хост магине
    for (int i = 0; i < numElements; ++i)
    {
        h_A[i] = rand()/(float)RAND_MAX;
        h_B[i] = rand()/(float)RAND_MAX;
    }

    // Выделяем место под вектор А на девайсе
    float *d_A = NULL;
    err = cudaMalloc((void **)&d_A, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    //  Выделяем место под вектор B на девайсе
    float *d_B = NULL;
    err = cudaMalloc((void **)&d_B, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Выделяем место под вектор C на девайсе
    float *d_C = NULL;
    err = cudaMalloc((void **)&d_C, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector C (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Копируем вектор A из памяти хоста в память девайса
    printf("Copy input data from the host memory to the CUDA device\n");
    err = cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector A from host to device (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
	// Копируем вектор B из памяти хоста в память девайса
    err = cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector B from host to device (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Запускаем суммирование векторов А и В на ядре CUDA 
    int threadsPerBlock = 256; // сколько нитей на блоке
    int blocksPerGrid =(numElements + threadsPerBlock - 1) / threadsPerBlock; // сколько блоков
    printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start); // запоминаем время начала
    vectorAdd<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, numElements); // передаем ядру CUDA количество блоков, нитей, размер массива и указатели на массивы в памяти девайса
	cudaEventRecord(stop);
	err = cudaGetLastError(); // читаем ошибки от CUDA
	cudaEventSynchronize(stop);
	float cuda_milliseconds = 0;
	cudaEventElapsedTime(&cuda_milliseconds, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
    if (err != cudaSuccess) // проверяем, нет ли ошибок выполнения
    {
        fprintf(stderr, "Failed to launch vectorAdd kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Копируем результирующий вектор из памяти девайса в память хоста
    printf("Copy output data from the CUDA device to the host memory\n");
    err = cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);

    if (err != cudaSuccess) // проверяем, не возниколо ли ошибок
    {
        fprintf(stderr, "Failed to copy vector C from device to host (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	time_t start_clock = clock();
	vectorAddCPU(h_A, h_B, h_C_CPU, numElements);
	time_t end_clock = clock();

    // Проверяем, что результирующий вектор вычислился верно
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(h_C_CPU[i] - h_C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification failed at element %d!\n", i);
            exit(EXIT_FAILURE);
        }
    }
	float cuda_milliseconds_CPU = (float)(end_clock - start_clock) / (CLOCKS_PER_SEC / 1000);
    printf("PASSED in %f for CUDA\n",cuda_milliseconds);
    printf("PASSED in %f for CPU\n", cuda_milliseconds_CPU);

    // Очищаем память девайса
    err = cudaFree(d_A);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    err = cudaFree(d_B);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector B (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    err = cudaFree(d_C);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector C (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Очищаем память хоста
    free(h_A);
    free(h_B);
    free(h_C);
	free(h_C_CPU);
    printf("Done\n");
    return 0;
}

