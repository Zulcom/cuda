﻿#include <stdio.h>
#include <cuda_runtime.h>
#include <cooperative_groups.h>
#include "helper_cuda.h"
#include <cstdlib>
#include <cmath>
#include <ctime>

void constantInit(float* data, int size, float val)
{
	for (int i = 0; i < size; ++i)
	{
		data[i] = val;
	}
}

void matrixMulCPU(float* C, float* A, float* B, size_t size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			float sum = 0.0;

			for (int k = 0; k < size; k++)
			{
				sum += A[k + i * size] * B[j + k * size];
			}

			C[j + i * size] = sum;
		}
	}
}

__global__ void
matrixMul(float* C, float* A, float* B, size_t size)
{
	int BLOCK_SIZE = 16;
	int i = threadIdx.x + blockIdx.x * BLOCK_SIZE;
	int j = threadIdx.y + blockIdx.y * BLOCK_SIZE;
	float sum = 0.0;

	for (int k = 0; k < size; k++)
	{
		sum += A[k + i * size] * B[j + k * size];
	}

	C[j + i * size] = sum;
}


__global__ void
matrixMulCUDA(float* C, float* A, float* B, unsigned int wA, unsigned int wB)
{
	const int BLOCK_SIZE = 16;
	// Handle to thread block group
	cooperative_groups::thread_block cta = cooperative_groups::this_thread_block();
	// Block index
	int bx = blockIdx.x;
	int by = blockIdx.y;

	// Thread index
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	// Index of the first sub-matrix of A processed by the block
	int aBegin = wA * BLOCK_SIZE * by;

	// Index of the last sub-matrix of A processed by the block
	int aEnd = aBegin + wA - 1;

	// Step size used to iterate through the sub-matrices of A
	int aStep = BLOCK_SIZE;

	// Index of the first sub-matrix of B processed by the block
	int bBegin = BLOCK_SIZE * bx;

	// Step size used to iterate through the sub-matrices of B
	int bStep = BLOCK_SIZE * wB;

	// Csub is used to store the element of the block sub-matrix
	// that is computed by the thread
	float Csub = 0;

	// Loop over all the sub-matrices of A and B
	// required to compute the block sub-matrix
	for (int a = aBegin, b = bBegin; a <= aEnd; a += aStep, b += bStep)
	{
		// Declaration of the shared memory array As used to
		// store the sub-matrix of A
		__shared__ float As[BLOCK_SIZE][BLOCK_SIZE];

		// Declaration of the shared memory array Bs used to
		// store the sub-matrix of B
		__shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

		// Load the matrices from device memory
		// to shared memory; each thread loads
		// one element of each matrix
		As[ty][tx] = A[a + wA * ty + tx];
		Bs[ty][tx] = B[b + wB * ty + tx];

		// Synchronize to make sure the matrices are loaded
		cooperative_groups::sync(cta);


		// Multiply the two matrices together;
		// each thread computes one element
		// of the block sub-matrix
#pragma unroll
		for (int k = 0; k < BLOCK_SIZE; ++k)
		{
			Csub += As[ty][k] * Bs[k][tx];
		}

		// Synchronize to make sure that the preceding
		cooperative_groups::sync(cta);
	}

	// Write the block sub-matrix to device memory;
	// each thread writes one element
	int c = wB * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + wB * ty + tx] = Csub;
}

/**
 * Host main routine
 */
int main(void)
{
	int block_size = 16;
	dim3 dimsA(5 * 1 * block_size, 5 * 1 * block_size, 1); // задаём размерность сетки блоков для матрицы А
	dim3 dimsB(5 * 1 * block_size, 5 * 1 * block_size, 1); // задаём размерность сетки блоков для матрицы А
	cudaError_t err = cudaSuccess;
	cudaEvent_t start, stop, start_S, stop_S;
	// Выделяем место под матрицы А и В на хосте
	unsigned int size_A = dimsA.x * dimsA.y;
	float cuda_milliseconds = 0, cuda_milliseconds_s = 0;
	unsigned int mem_size_A = sizeof(float) * size_A;
	float* h_A = (float *)malloc(mem_size_A);
	unsigned int size_B = dimsB.x * dimsB.y;
	unsigned int mem_size_B = sizeof(float) * size_B;
	float* h_B = (float *)malloc(mem_size_B);
	// Выделяем место под матрицу С на хосте
	dim3 dimsC(dimsB.x, dimsA.y, 1);
	printf("Matrix size %d", dimsB.x);
	unsigned int mem_size_C = dimsC.x * dimsC.y * sizeof(float);
	float* h_C = (float *)malloc(mem_size_C);
	float* h_S = (float *)malloc(mem_size_C);
	float* h_C_CPU = (float *)malloc(mem_size_C);
	if (h_C == NULL) // проверяем, хватило ли место под матрицу С
	{
		fprintf(stderr, "Failed to allocate host matrix C!\n");
		exit(EXIT_FAILURE);
	}
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventCreate(&start_S);
	cudaEventCreate(&stop_S);

	// Запоняем матрицы
	const float valB = 0.01f;

	constantInit(h_A, size_A, 1.0f);
	constantInit(h_B, size_B, valB);

	// Создаём указатели на матрицы на девайсе
	float *d_A = NULL, *d_B = NULL, *d_C = NULL, *d_S = NULL;
	err = cudaMalloc((void **)&d_A, mem_size_A); // выделяем место под матрицу А на девайсе

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	//  Выделяем место под вектор B на девайсе
	err = cudaMalloc((void **)&d_B, mem_size_B); // выделяем место под матрицу В на девайсе

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	// Выделяем место под вектор C на девайсе
	err = cudaMalloc((void **)&d_C, mem_size_C); // выделяем место под матрицу С на девайсе

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to allocate device vector C (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	// Выделяем место под вектор S на девайсе
	err = cudaMalloc((void **)&d_S, mem_size_C); // выделяем место под матрицу S на девайсе

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to allocate device vector C (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	err = cudaMemcpy(d_A, h_A, mem_size_A, cudaMemcpyHostToDevice); // копируем матрицу А в память девайса
	err = cudaMemcpy(d_B, h_B, mem_size_B, cudaMemcpyHostToDevice); // копируем матрицу В в память девайса

	// Натсраиваем параметры запуска
	dim3 threads(block_size, block_size);
	dim3 grid(dimsB.x / threads.x, dimsA.y / threads.y);

	// Запускаем перемножение на ядре CUDA 
	cudaFuncSetCacheConfig(matrixMulCUDA, cudaFuncCachePreferShared);
	cudaEventRecord(start); // запоминаем время начала
	matrixMulCUDA<<<grid,threads>>>(d_C, d_A, d_B, dimsA.x, dimsB.x);
	// передаем ядру CUDA количество блоков, нитей, размер массива и указатели на массивы в памяти девайса
	cudaEventRecord(stop); 
	cudaEventSynchronize(stop); cudaEventElapsedTime(&cuda_milliseconds, start, stop);
	err = cudaGetLastError(); // читаем ошибки от CUDA

	if (err != cudaSuccess) // проверяем, нет ли ошибок выполнения
	{
		fprintf(stderr, "Failed to launch vectorAdd kernel (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	// Копируем результирующий вектор из памяти девайса в память хоста
	printf("Copy output data from the CUDA device to the host memory\n");
	err = cudaMemcpy(h_C, d_C, mem_size_C, cudaMemcpyDeviceToHost);

	if (err != cudaSuccess) // проверяем, не возниколо ли ошибок
	{
		fprintf(stderr, "Failed to copy vector C from device to host (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}


	cudaEventRecord(start_S); // запоминаем время начала
	matrixMul<<<grid, threads >>>(d_S, d_A, d_B, dimsA.x);
	// передаем ядру CUDA количество блоков, нитей, размер массива и указатели на массивы в памяти девайса
	cudaEventRecord(stop_S); cudaEventSynchronize(stop_S); cudaEventElapsedTime(&cuda_milliseconds_s, start_S, stop_S);
	err = cudaGetLastError(); // читаем ошибки от CUDA

	// Копируем результирующий вектор из памяти девайса в память хоста
	err = cudaMemcpy(h_S, d_S, mem_size_C, cudaMemcpyDeviceToHost);

	if (err != cudaSuccess) // проверяем, не возниколо ли ошибок
	{
		fprintf(stderr, "Failed to copy vector S from device to host (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	time_t start_clock = clock();
	matrixMulCPU(h_C_CPU, h_A, h_B, dimsA.x);
	time_t end_clock = clock();

	bool correct = true;

	double eps = 1.e-6; // machine zero

	for (int i = 0; i < (int)(dimsC.x * dimsC.y); i++)
	{
		double rel_err = fabs(h_C_CPU[i] - h_S[i]);

		if (rel_err > eps)
		{
			printf("Error! Matrix[%05d]=%.8f, ref=%.8f error term is > %E\n", i, h_C[i], dimsA.x * valB, eps);
			correct = false;
		}
	}
	printf("%s\n", correct ? "Result = PASS" : "Result = FAIL");
	float cpu_milliseconds = (float)(end_clock - start_clock) / (CLOCKS_PER_SEC / 1000);
	printf("Shared CUDA in %f", cuda_milliseconds);
	printf("\nSimple CUDA in %f", cuda_milliseconds_s);
	printf("\nCPU  in %f", cpu_milliseconds);
	// Очищаем память девайса
	err = cudaFree(d_A);

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to free device vector A (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	err = cudaFree(d_B);

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to free device vector B (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	err = cudaFree(d_C);

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to free device vector C (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	err = cudaFree(d_S);

	if (err != cudaSuccess)
	{
		fprintf(stderr, "Failed to free device vector S (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	// Очищаем память хоста
	free(h_A);
	free(h_B);
	free(h_C);
	free(h_S);
	free(h_C_CPU);
	return 0;
}
